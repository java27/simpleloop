package simpleLoop;

public class ProcessWidget {

  private double progressPercentage = 0;

  /**
   * This method is to set the progressPercentage whenever the onProgressUpdate is called.
   * @param progressPercentage
   */

  public void setProgress(double progressPercentage) {
    this.progressPercentage = progressPercentage;
  }

  /**
   * This function displays the progress bar and returns the output string.
   * @return
   */

  public String display() {
    final int width = 100;
    String output = "[";
    int i = 0;
    for (; i <= (int) (this.progressPercentage * width); i++) {
      output += ".";
      //System.out.println("uauauau");
    }
    for (; i < width; i++) {
      output += " ";
    }
    output += "]";
    return output;
  }
}
