package simpleLoop;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ProgressMessage implements Message {


 public float value;
 public String type;

 /**
  * This function sets the Message of ProgressMessage this.type = "progress"
  * @param value
  */

 public ProgressMessage(float value){

  this.value = value;
  this.type = "progress";
 }


}
