package simpleLoop;


import java.util.Date;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SimpleLoop {
  private boolean isRunning = true;
  private ProcessWidget processWidget;
  private ConcurrentLinkedQueue<Message> queue = new ConcurrentLinkedQueue<>();


  public static void main(String[] args) {
    SimpleLoop loop = new SimpleLoop();
    try {
      loop.run();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public SimpleLoop(){
    processWidget = new ProcessWidget();
  }

  /**
   * In this function we have created our worker thread which implements the very long task. Progress Message is added in the queue to notify the progress of the very long task
   *at the end of the very long task completion Message is added.
   * On the other hand the timer is running and continuously updating. Before updating the timer we are checking for the completion message
   * and accordingly call OnComplete Method or Progress Method
   *  @throws InterruptedException
   */
  void run() throws InterruptedException {
    // trigger thread
    Thread ok = new Thread() {
      public void run() {

        for (int i = 0; i < 100; i++) {

          final float progess = (float) i / 100;
           queue.add(new ProgressMessage(progess));




          try {
            Thread.sleep(100);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }

         queue.add(new CompletionMessage());

      }
    };
  ok.start();

    while (isRunning) {

      Message message = queue.poll();

      if (message != null)
      {
        if (message instanceof CompletionMessage)
        {
          onComplete();
        }
        else
          onProgressUpdate(((ProgressMessage) message).value);

      }
      update();
      Thread.sleep(20);
    }
  }


  /**
   * This method updates the timer and display it on the run window
   */
  private void update(){
    System.out.print("\r" + processWidget.display() + " " + new Date());
  }

  /**
   * This function is called based on the message received and it updates the progress percentage of the task
   * @param i
   */

  void onProgressUpdate( float i)
  {
    this.processWidget.setProgress(i);
  }

  /**
   * This function is called based on the message received and it stops the timer by making this.isRunning = false
   */
  void onComplete() {
     System.out.println("Task Complete");
    this.isRunning = false;
  }
}
